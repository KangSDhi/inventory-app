<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
<div class="relative p-2 bg-base-200">
    <div class="navbar mb-2 bg-neutral text-neutral-content rounded-box">
        <div class="flex-1 px-2 mx-2">
            <a href="{{ route('get.homeIndex') }}">
                <span class="text-lg font-bold">
                    Inventory App
                </span>
            </a>
        </div>
        <div class="flex-none hidden px-2 mx-2 lg:flex">
            <div class="flex items-stretch">
                <a class="btn btn-ghost btn-sm rounded-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" class="inline-block w-5 mr-2 stroke-current" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                    </svg>
                    Tamu
                </a>
                <a href="{{ route('get.authAdminLogin') }}" class="btn btn-ghost btn-sm rounded-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" class="inline-block w-5 mr-2 stroke-current" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" />
                    </svg>
                    Login
                </a>
            </div>
        </div>
    </div>
    @yield('content')
    <footer class="p-4 footer bg-neutral rounded-box text-base-content footer-center">
        <div class="text-neutral-content">
            <p>Copyright © 2021 - All right reserved by KangSDhi</p>
        </div>
    </footer>
</div>
</body>
</html>
